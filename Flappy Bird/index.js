var parentDiv = document.createElement("div");


var canvasDiv = document.createElement("div");
var questionDiv = document.createElement("div");
var scoreDiv = document.createElement("div");
parentDiv.appendChild(questionDiv);
parentDiv.appendChild(canvasDiv);
parentDiv.appendChild(scoreDiv);

questionDiv.id = "questionDiv";

scoreDiv.id = "scoreDiv"

canvasDiv.id = "canvasDiv";

document.body.appendChild(parentDiv);

var canvas = document.createElement("canvas");
var context = canvas.getContext("2d");
canvas.width = 600;
canvas.height = 480;
canvas.style.border = "solid 1px #000";

var questionLabel = document.createElement("label");
questionLabel.style.padding = "20px";
questionDiv.appendChild(questionLabel);

canvasDiv.appendChild(canvas);

var scoreLabel = document.createElement("label");
scoreLabel.style.padding = "20px";
scoreDiv.appendChild(scoreLabel);


var birdImageHeight = 50;
var birdImageWidth = 75;
var pipeImageHeight = 150;
var pipeImageWidth = 50;
var pipeMoveLeftSpeed = 80; 
var birdMoveUpSpeed = 225;
var birdMoveDownSpeed = 225;
var spacerRequirement = 250;
var questionPipeHeight = 160;
var questionPipeLocation = 160;

var requiredScore;
var birdX = 100;

var questions = [{question : "Does red and blue make purple?", answer : false}, {question : "|1 + 2i| < 3 ?", answer : true}, {question : "Na + OH = NaOH?", answer : false}, {question : "F = MA ?", answer : true}, {question : "Is New Delhi the capital of India?", answer : true}];

requiredScore = Math.round(0.8 * questions.length);
var wrongMaximum = questions.length - requiredScore;



var pipeUpReady = false;
var pipeUpImage = new Image();

pipeUpImage.onload = function() {
	pipeUpReady = true;
}

pipeUpImage.src = "pipeUp.png";

var pipeDownReady = false;
var pipeDownImage = new Image();

pipeDownImage.onload = function() {
	pipeDownReady = true;
}

pipeDownImage.src = "pipeDown.png";

var pipeQuestionReady = false;
var pipeQuestionImage = new Image();

pipeQuestionImage.onload = function() {
	pipeQuestionReady = true;
}

pipeQuestionImage.src = "questionPipe.png";

var trueReady = false;
var trueImage = new Image();

trueImage.onload = function() {
	trueReady = true;
}

trueImage.src = "true1.png";

var falseReady = false;
var falseImage = new Image();

falseImage.onload = function() {
	falseReady = true;
}

falseImage.src = "false1.png";

var birdReady = false;
var birdImage = new Image();

birdImage.onload = function() {
	birdReady = true;
}

birdImage.src = "submarine.png";

var bird = {
	y : 240
};

var pipes = [];

var start = false;

var moveUp = false;
canvas.addEventListener('mousedown', function () {
	moveUp = true;
});

canvas.addEventListener('mouseup', function () {
	moveUp = false;
});


var reset = function (message) {
	pipes = [];
	bird.y = 240;
	moveUp = false;
	previous = 0;
	spacer = 0.0
	counter = 0;
	nextQuestion = true;
	questionLabel.innerHTML = "The questions will go here."; 
	scoreLabel.innerHTML = "Score : 0"
	score = 0;
	questionPointer = 0;
	wrongScore = 0;
	
	context.beginPath();
	context.rect(0.0, 0.0, canvas.width, canvas.height);
	context.fillStyle = "rgb(80, 117, 255)";
	context.fill();
 
	if (!start) {
		context.font = 'italic 20pt Calibri';
		context.strokeText(message, 75, 240);
		var button = document.createElement("button");
		var text = document.createTextNode("Start");
		button.appendChild(text);

		var label = document.createElement("label");
		label.innerHTML = "<p> <p>";

		parentDiv.appendChild(label);
		parentDiv.appendChild(button);

		button.addEventListener("click", function () {
			start = true;
			parentDiv.removeChild(button);
			main();
		}); 
	}
};

var spacer = 0.0;
var previous = 0;
var counter = 0;

var nextQuestion = false;
var questionPointer = 0;

var score = 0;
var wrongScore = 0;

var update = function (modifier) { 

	if (moveUp) {
		bird.y -= modifier * birdMoveUpSpeed;
	}
	else {
		bird.y += modifier * birdMoveDownSpeed;
	}

	for (var i = 0; i < pipes.length; i++) {
		pipes[i].x -= modifier * pipeMoveLeftSpeed;
	}

	if (spacer > spacerRequirement) {
		if (Math.random() > 0.2) {

			if (counter > 2 && nextQuestion) {
				pipes.push({
					x : canvas.width,
					orientation : 'question',
					height : questionPipeHeight,
					answer : questions[questionPointer].answer
				});
				counter = -1;
				questionLabel.innerHTML = questions[questionPointer].question;
				nextQuestion = false;
				questionPointer++;
			}

			else if (Math.random() > 0.5 + (previous / 10.0)) {
				pipes.push({
					x : canvas.width,
					orientation : 'up',
					height : Math.round((Math.random() * 100) + 150)
				});
				previous++;
			}

			else {
				pipes.push({ 
					x : canvas.width,
					orientation : 'down',
					height : Math.round((Math.random() * 100) + 150)
				});
				previous--;
			};

		};
		spacer = 0.0;
		counter++;
	;}

	
	if (pipes.length > 0 && pipes[0].x + pipeImageWidth < 0.0) {
		pipes.shift();
	} 

	if (bird.y + 5.0 < 0.0 || bird.y + 45.0 > canvas.height) {
		start = false;
		reset("Do you want to restart?");
		return;
	}

	if ((score + wrongScore == questions.length) && score < requiredScore) {
		start = false;
		reset("You did not meet the required score.");
		return;
	}
	if ((score + wrongScore == questions.length) && score >= requiredScore) {
		start = false;
		reset("You did it!.");
		return;
	}

	if (wrongScore > wrongMaximum) {
		start = false;
		reset("You missed too many questions.");
		return;
	}

	for (var i = 0; i < pipes.length; i++) {
		if (pipes[i].x + pipeImageWidth >= birdX + birdImageWidth && pipes[i].x <= birdX + birdImageWidth) {
			if (pipes[i].orientation == 'up' && (canvas.height >= bird.y && bird.y >= (canvas.height - pipes[i].height))) {
				start = false;
				reset("Do you want to restart?");
				return;
			}

			if (pipes[i].orientation == 'down' && (0.0 <= bird.y && bird.y <= pipes[i].height)) {
				start = false;
				reset("Do you want to restart?");
				return;
			}

			if (pipes[i].orientation == "question") {
				if (questionPipeLocation <= bird.y && ((questionPipeLocation + questionPipeHeight) >= bird.y)) {
					start = false;
					reset("Do you want to restart?");
					return;
				}
				if (!nextQuestion && (bird.y < questionPipeLocation) && pipes[i].answer) {
					score++;
					pipeMoveLeftSpeed += 30;
					scoreLabel.innerHTML = "Score : " + score;
					nextQuestion = true;
					var timer = 0;
					var interval = setInterval(function () {
						scoreDiv.style.backgroundColor = scoreDiv.style.backgroundColor == "green" ? "#e6e6e6" : "green";
						if (timer > 10) {
							clearInterval(interval);
							scoreDiv.style.backgroundColor = "#e6e6e6"
						}
						timer++;
					}, 250);
				}
				else if (!nextQuestion && ((questionPipeLocation + questionPipeHeight) < bird.y) && (!pipes[i].answer)){
					score++;
					pipeMoveLeftSpeed += 30;
					scoreLabel.innerHTML = "Score : " + score;
					nextQuestion = true;
					var timer = 0;
					var interval = setInterval(function () {
						scoreDiv.style.backgroundColor = scoreDiv.style.backgroundColor == "green" ? "#e6e6e6" : "green";
						if (timer > 10) {
							clearInterval(interval);
							scoreDiv.style.backgroundColor = "#e6e6e6"
						}
						timer++;
					}, 250);
				}
				else if (!nextQuestion) {
					scoreLabel.innerHTML = "Score : " + score;
					nextQuestion = true;
					wrongScore++;
					var timer = 0;
					var interval = setInterval(function () {
						scoreDiv.style.backgroundColor = scoreDiv.style.backgroundColor == "red" ? "#e6e6e6" : "red";
						if (timer > 10) {
							clearInterval(interval);
							scoreDiv.style.backgroundColor = "#e6e6e6"
						}
						timer++;
					}, 250);
				}
			}   
		}
	}


	spacer += modifier * pipeMoveLeftSpeed;

}


var render = function () {

	context.beginPath();
	context.rect(0.0, 0.0, canvas.width, canvas.height);
	context.fillStyle = "rgba(80, 117, 255)";
	context.fill(); 

	if (pipeUpReady && pipeDownReady && pipeQuestionReady && trueReady && falseReady) {
		for (var i = 0; i < pipes.length; i++) {
			if (pipes[i].orientation == "up") {
				context.drawImage(pipeUpImage, pipes[i].x, canvas.height - pipes[i].height, pipeImageWidth, pipes[i].height);
			}
			else if (pipes[i].orientation == "down") {
				context.drawImage(pipeDownImage, pipes[i].x, 0.0, pipeImageWidth, pipes[i].height); 
			}
			else {
				context.drawImage(pipeQuestionImage, pipes[i].x, questionPipeLocation , pipeImageWidth, pipes[i].height);
				context.drawImage(trueImage, pipes[i].x, 0, pipeImageWidth, pipes[i].height); 
				context.drawImage(falseImage, pipes[i].x, questionPipeLocation + questionPipeHeight, pipeImageWidth, pipes[i].height); 
			}
		}
	}

	if (birdReady) {
		context.drawImage(birdImage, birdX, bird.y, birdImageWidth, birdImageHeight);
	}

}


var main = function () {
	var now = Date.now();
	var delta = now - then;
	if (start) {
		render();
		update(delta / 1000);
	};

	then = now;		
	requestAnimationFrame(main);
};


var then = Date.now();
reset("Do you want to start?");
main();

















































