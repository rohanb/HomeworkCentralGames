var parentDiv = document.createElement("div");


var canvasDiv = document.createElement("div");
var questionDiv = document.createElement("div");
var scoreDiv = document.createElement("div");
parentDiv.appendChild(questionDiv);
parentDiv.appendChild(canvasDiv);
parentDiv.appendChild(scoreDiv);

questionDiv.id = "questionDiv";

scoreDiv.id = "scoreDiv"

canvasDiv.id = "canvasDiv";

document.body.appendChild(parentDiv);

var questionLabel = document.createElement("label");
questionDiv.appendChild(questionLabel);

var canvas = document.createElement("canvas");
var context = canvas.getContext("2d");
canvas.height = 480;
canvas.width = 1000;
canvasDiv.appendChild(canvas);

realImageReady = false;
realImage = new Image();
realImage.onload = function() {
	realImageReady = true;
}

realImage.src = "real.png";

imageDownImageReady = false;
imageDownImage = new Image();
imageDownImage.onload = function() {
	imageDownImageReady = true;
}

imageDownImage.src = "imageDown.png";

imageUpImageReady = false;
imageUpImage = new Image();
imageUpImage.onload = function() {
	imageUpImageReady = true;
}

imageUpImage.src = "imageUp.png";

baseImageReady = false;
baseImage = new Image();
baseImage.onload = function() {
	baseImageReady = true;
}

baseImage.src = "base.png";

var displayChanged = false;

var mouseDown = false;

var realImageX = 100.0;
var realImageHeight = 25.0;
var realImageWidth = 25.0;
var realImageY = 215.0;

var realFocusX = 600.0;
var realFocusY = 240.0;

var centreX = 500.0;
var centreY = 240.0;

var virtualFocusX = 400.0;
var virtualFocusY = 240.0;

var focalAxisX1 = 500.0;
var focalAxisY1 = 0.0;
var focalAxisX2 = 500.0;
var focalAxisY2 = 480.0;

var imageWidth = 25;
var imageHeight;

questions = {question : "Produce an Image that is real, same size as the object and inverted.", answer : "2"};

canvas.addEventListener("mousedown", function (event) {
	var mousePosition = getMousePosition(event);
	if ((realImageX <= mousePosition.x && mousePosition.x <= (realImageX + realImageWidth)) && (realImageY <= mousePosition.y && mousePosition.y <= (realImageY + realImageHeight))) {	
		mouseDown = true;

	}
		
});

canvas.addEventListener("mouseup", function(event) {
	
	mouseDown = false;
	var mousePosition = getMousePosition(event);
	mousePosition.x = (mousePosition.x + (realImageWidth / 2));
	if (mousePosition.x >= 385 && mousePosition.x <= 415) {
		realImageX = 400 - (realImageWidth / 2);
		setOption(3);
	}

	if (mousePosition.x >= 285 && mousePosition.x <= 315) {
		realImageX = 300 - (realImageWidth / 2);
		setOption(1);
	}
	
	if (mousePosition.x < 285) {
		setOption(0);
	}

	if (mousePosition.x > 315 && mousePosition.x < 385) {
		setOption(2);
	}

	if (mousePosition.x > 415) {
		setOption(4);
	}

});

canvas.addEventListener("mousemove", function(event) {
	if (mouseDown) {
		var mousePosition = getMousePosition(event);
		var mouseX = mousePosition.x;
		if ((mouseX + (realImageWidth / 2)) < focalAxisX1) {
			realImageX = mouseX;
			displayChanged = true;
		}
		
	}
});

var getMousePosition = function (event) {
	var rectangle = canvas.getBoundingClientRect();
	return {
	x : event.clientX - rectangle.left,
	y : event.clientY - rectangle.top
	};

}

var drawBase = function () {

	context.fillStyle = "white";
	context.fillRect(0, 0, canvas.width, canvas.height);

	context.lineWidth = "1";
	context.strokeStyle = "black";
	context.moveTo(500,0);
	context.quadraticCurveTo(480, 240, 500, 480);
	context.stroke()

	context.lineWidth = "1";
	context.strokeStyle = "black";
	context.moveTo(500,0);
	context.quadraticCurveTo(520, 240, 500, 480);
	context.stroke()

	context.lineWidth = "1";
	context.strokeStyle = "black";
	context.beginPath();
	context.moveTo(0, 240);
	context.lineTo(1000, 240);
	context.stroke()

	context.lineWidth = "1";
	context.strokeStyle = "black";
	context.beginPath();
	context.moveTo(500, 0);
	context.lineTo(500, 480);
	context.stroke()

	context.lineWidth = "1";
	context.strokeStyle = "black";
	context.beginPath();
	context.moveTo(300, 240);
	context.lineTo(300, 248);
	context.stroke()

	context.lineWidth = "1";
	context.strokeStyle = "black";
	context.beginPath();
	context.moveTo(400, 240);
	context.lineTo(400, 248);
	context.stroke()

	context.lineWidth = "1";
	context.strokeStyle = "black";
	context.beginPath();
	context.moveTo(600, 240);
	context.lineTo(600, 248);
	context.stroke()

	context.lineWidth = "1";
	context.strokeStyle = "black";
	context.beginPath();
	context.moveTo(700, 240);
	context.lineTo(700, 248);
	context.stroke()

	context.lineWidth = "1";
	context.strokeStyle = "black";
	context.beginPath();
	context.moveTo(200, 240);
	context.lineTo(200, 248);
	context.stroke()

	context.lineWidth = "1";
	context.strokeStyle = "black";
	context.beginPath();
	context.moveTo(100, 240);
	context.lineTo(100, 248);
	context.stroke()

	context.lineWidth = "1";
	context.strokeStyle = "black";
	context.beginPath();
	context.moveTo(5, 240);
	context.lineTo(5, 248);
	context.stroke()

	context.lineWidth = "1";
	context.strokeStyle = "black";
	context.beginPath();
	context.moveTo(800, 240);
	context.lineTo(800, 248);
	context.stroke()

	context.lineWidth = "1";
	context.strokeStyle = "black";
	context.beginPath();
	context.moveTo(900, 240);
	context.lineTo(900, 248);
	context.stroke()

	context.lineWidth = "1";
	context.strokeStyle = "black";
	context.beginPath();
	context.moveTo(995, 240);
	context.lineTo(995, 248);
	context.stroke()

	context.fillStyle = "black";	

	context.font = "10px Arial";
	context.fillText("F", 395, 260);
	context.stroke()

	context.font = "10px Arial";
	context.fillText("2F", 295, 260);
	context.stroke()

	context.font = "10px Arial";
	context.fillText("F", 595, 260);
	context.stroke()

	context.font = "10px Arial";
	context.fillText("2F", 695, 260);
	context.stroke()

	context.font = "10px Arial";
	context.fillText("3F", 195, 260);
	context.stroke()

	context.font = "10px Arial";
	context.fillText("3F", 795, 260);
	context.stroke()

	context.font = "10px Arial";
	context.fillText("4F", 95, 260);
	context.stroke()

	context.font = "10px Arial";
	context.fillText("4F", 895, 260);
	context.stroke()

	context.font = "10px Arial";
	context.fillText("5F", 5, 260);
	context.stroke()

	context.font = "10px Arial";
	context.fillText("5F", 985, 260);
	context.stroke()

}

var setOption = function (number) {

	var options = document.getElementsByName("answer");
	options[number].checked = true;

}

var findIntersection = function (x1, y1, x2, y2, x3, y3, x4, y4) {

	m1 = (y2 - y1) / (x2 - x1);
	m2 = (y4 - y3) / (x4 - x3);

	if (m1 == m2) {
		return null;
	}

	if (m1 === Infinity) {
		var xConst = x1;
		var y = y3 + m2 * (xConst - x3);
		return {x : xConst, y : y}; 
	}

	if (m2 === Infinity) {
		var xConst = x3;
		var y = y1 + m1 * (xConst - x1);
		return {x : xConst, y : y}; 
	}

	if (m1 === 0.0) {
		var yConst = y1;
		var x = ((yConst - y3) / m2) + x3;
		return {x : x, y : yConst}; 
	}

	if (m2 === 0.0) {
		var yConst = y3;
		var x = ((yConst - y1) / m1) + x1;
		return {x : x, y : yConst}; 
	}

	var x = ((y3 - y1) + (m1 * x1 - m2 * x3)) / (m1 - m2);
	var y = m1 * x - m1 * x1 + y1;

	return {x : x, y : y}; 

}

var findImagePosition = function (realX, realY) {

	var virtualRulePoint = findIntersection(realX, realY, virtualFocusX, virtualFocusY, focalAxisX1, focalAxisY1, focalAxisX2, focalAxisY2);
	if (virtualRulePoint !== null) {
		var firstPoint = findIntersection(realX, realY, centreX, centreY, virtualRulePoint.x, virtualRulePoint.y, 1000, virtualRulePoint.y);
		var firstPointX = firstPoint.x;
		var firstPointY = firstPoint.y;
	}

	var secondRulePoint = findIntersection(realX, realY, focalAxisX1, realY, focalAxisX1, focalAxisY1, focalAxisX2, focalAxisY2);
	var secondIntersection = findIntersection(realX, realY, centreX, centreY, secondRulePoint.x, secondRulePoint.y, realFocusX, realFocusY);


	var imageWidth = realImageHeight * ((firstPointX < focalAxisX1 ? (240 - firstPointY) : (firstPointY - 240)) / realImageHeight)
	drawLine(realX + realImageWidth / 2, realY, virtualFocusX, virtualFocusY);

	if (virtualRulePoint != null) {
		drawLine(virtualFocusX, virtualFocusY, virtualRulePoint.x, virtualRulePoint.y);
		drawLine(realX + realImageWidth / 2, realY, firstPoint.x + imageWidth / 2, firstPoint.y);
		drawLine(virtualRulePoint.x, virtualRulePoint.y, firstPoint.x + imageWidth / 2, firstPoint.y);
		drawLine(realX + realImageWidth / 2, realY, secondRulePoint.x, secondRulePoint.y);

		if (secondIntersection !== null) {
				drawLine(secondRulePoint.x, secondRulePoint.y, secondIntersection.x + imageWidth / 2, secondIntersection.y);
		}

	}

	return {x : firstPointX, y : firstPointY};

};

var drawLine = function (x1, y1, x2, y2) {
	
	context.strokeStyle = "black";
	context.lineWidth = "1px";
	context.beginPath();
	context.moveTo(x1, y1);
	context.lineTo(x2, y2);
	context.stroke();
}

var checkAnswer = function () {
	var options = document.getElementsByName("answer");
	var correct = questions.answer;
	for (var i = 0; i < options.length; i++) {
		console.log(options[i].value);
		if (options[i].checked) {
			if (options[i].value == correct) {									
				scoreDiv.style.backgroundColor = "green";		
			}
			else {
				scoreDiv.style.backgroundColor = "red";		
			}
		}	
	}		

} 

var reset = function () {

	questionLabel.innerHTML = questions.question;
	scoreDiv.innerHTML = "<form class = 'form-inline' role = 'form' onsubmit = 'checkAnswer();return false'> " +
"<input type = 'radio' name = 'answer' value = '1'> Beyond 2F      </input>" +
"<input type = 'radio' name = 'answer' value = '2'> At 2F      </input>" + 
"<input type = 'radio' name = 'answer' value = '3'> Between 2F and F      </input>" +
"<input type = 'radio' name = 'answer' value = '4'> At F      </input>" +
"<input type = 'radio' name = 'answer' value = '5'> Between F and the Lens     </input>" +
"<button name = 'submit' value = 'submit' id = 'submit'>  </input>" + 
"</form>"
	displayChanged = true;
	var realImageX = 250;
	var text = document.createTextNode('Submit');
	document.getElementById('submit').appendChild(text);

}

var update = function (modifier) {

}

var render = function () {

	drawBase();
	displayChanged = false;
	var imagePosition = findImagePosition(realImageX, realImageY);
	var imagePositionX = imagePosition.x;
	var imagePositionY = imagePosition.y;

	if (realImageReady) {
		context.drawImage(realImage, realImageX, realImageY, realImageWidth, realImageHeight);
		context.font = "10px Arial";
		context.fillText("Object", realImageX, realImageY - 20);
		context.stroke()
	}

	if (imageDownImageReady && imageUpImageReady) {

		if (imagePositionX >= focalAxisX1) {
			context.drawImage(imageDownImage, imagePositionX, 240, realImageWidth * ((imagePositionY - 240) / realImageHeight), imagePositionY - 240);
			context.font = "10px Arial";
			context.fillText("Virtual Image", imagePositionX, imagePositionY + 20);
			context.stroke()
		}

		if (imagePositionX < focalAxisX1) {
			context.drawImage(imageUpImage, imagePositionX, imagePositionY, realImageWidth * ((240 - imagePositionY) / realImageHeight), 240 - imagePositionY);		          
			context.font = "10px Arial";
			context.fillText("Virtual Image", imagePositionX, imagePositionY - 20);
			context.stroke()
		}

		
	}

}

var main = function() {

	var now = Date.now();
	var delta = now - then;
	update(delta/1000);
	
	render();

	then = now;

	requestAnimationFrame(main);
}

var then = Date.now;
reset();
main();

	























